FROM golang


ADD ./ /go/src/infnotesapi
RUN go get github.com/tools/godep 
RUN cd /go/src/infnotesapi && godep restore
RUN go install infnotesapi
RUN mkdir -p /go/bin/database/
WORKDIR /go/src/infnotesapi/
ENTRYPOINT /go/bin/infnotesapi

EXPOSE 8080