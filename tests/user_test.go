package test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"runtime"
	"path/filepath"
	_ "infnotesapi/routers"
	"bytes"
	"infnotesapi/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	_, file, _, _ := runtime.Caller(1)
	apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".." + string(filepath.Separator))))
	// register model
	orm.RegisterModel(new(models.User), new(models.Note))
	orm.RegisterDriver("sqlite", orm.DR_Sqlite)
	orm.RegisterDataBase("default", "sqlite3", "../database/orm_tests.db")

	orm.RunSyncdb("default", true, false)

	beego.TestBeegoInit(apppath)
}
// TestLogin test sending empty login POSD
func TestLogin(t *testing.T) {
	r, _ := http.NewRequest("POST", "/v1/users/login", bytes.NewBuffer([]byte(`{}`)))
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Sending empty login POST\n", t, func() {
			Convey("Status Code Should Be 401", func() {
					So(w.Code, ShouldEqual, 401)
				})
			Convey("The Result Should Be error response", func() {
					So(w.Body.String(), ShouldEqual, "{\n" +
								"  \"error\": \"Missing email or password\"\n" +
								"}")
				})
		})
}

// TestLogin is a sample to run an endpoint test
func TestLoginSuccess(t *testing.T) {
	var jsonStr = []byte(`{"email":"test@test.pl", "password": "a"}`)

	r, _ := http.NewRequest("POST", "/v1/users/login", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Sending standard login POST should create an account\n", t, func() {
			Convey("Status Code Should Be 200", func() {
					So(w.Code, ShouldEqual, 200)
				})
			Convey("The Result Should Be error response", func() {
					So(w.Body.String(), ShouldEqual, "{\n" +
								"  \"token\": \"8fHLFT+/sYmyVA/g+v/3dB0uSJg=\"\n" +
								"}")
				})
		})
}

// TestLogin is a sample to run an endpoint test
func TestLoginWrongPassword(t *testing.T) {
	var jsonStr = []byte(`{"email":"test@test.pl", "password": "bbbb"}`)

	r, _ := http.NewRequest("POST", "/v1/users/login", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Sending wrong passwords to existing account should fail\n", t, func() {
			Convey("Status Code Should Be 401", func() {
					So(w.Code, ShouldEqual, 401)
				})
			Convey("The Result Should Be error response", func() {
					So(w.Body.String(), ShouldEqual, "{\n" +
								"  \"error\": \"Wrong creditentials\"\n" +
								"}")
				})
		})
}

