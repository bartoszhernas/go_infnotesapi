package routers

import (
	"github.com/astaxie/beego"
)

func init() {
	
	beego.GlobalControllerRouter["infnotesapi/controllers:UserController"] = append(beego.GlobalControllerRouter["infnotesapi/controllers:UserController"],
		beego.ControllerComments{
			"Login",
			`/login`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["infnotesapi/controllers:UserController"] = append(beego.GlobalControllerRouter["infnotesapi/controllers:UserController"],
		beego.ControllerComments{
			"Logout",
			`/logout`,
			[]string{"post"},
			nil})

}
