package controllers

import (
	"infnotesapi/models"
	"github.com/astaxie/beego"
	"encoding/json"
	"fmt"
)

// Operations about Users
type UserController struct {
	beego.Controller
}

type LoginRequest struct {
	Email string
	Password string
}


// @Title login
// @Description Logs user into the system
// @Param	username		body 	string	true		"The email for login"
// @Param	password		body 	string	true		"The password for login"
// @Success 200 {string} lonin success
// @Failure 401 unauthenticated
// @router /login [post]
func (u *UserController) Login() {
	fmt.Printf("Trying to log in with body: %s \n", u.Ctx.Request.Body)
	decoder := json.NewDecoder(u.Ctx.Request.Body)
	var request_data LoginRequest
	decoder.Decode(&request_data)

	response := make(map[string]string)

	email := request_data.Email

	password := request_data.Password

	fmt.Printf("Trying to log in with email: %s \n", email)

	if len(email) == 0 || len(password) == 0 {
		response["error"] = "Missing email or password"
		u.Ctx.ResponseWriter.WriteHeader(401)
	} else {
		logged, token := models.Login(email, password)
		if logged {
			response["token"] = token
		} else {
			response["error"] = "Wrong creditentials"
			u.Ctx.ResponseWriter.WriteHeader(401)
		}
	}

	u.Data["json"] = response
	u.ServeJson()
}

// @Title logout
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [post]
func (u *UserController) Logout() {
	u.Data["json"] = "logout success"
	u.ServeJson()
}

