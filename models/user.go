package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"crypto/sha1"
	"encoding/base64"
)

func init() {
}

type User struct {
	Id   int
	Email string `orm:"size(255);index"`
	Password string `orm:"size(28)"`
	Token string `orm:"size(28);index"`
	Notes []*Note `orm:"reverse(many)"`
}

func AddUser(u User) string {
	return " "
}

func GetUserByToken(token string) (u *User, err error) {
	var user User
	o := orm.NewOrm()

	dbErr := o.QueryTable("user").Filter("token", token).One(&user)
	return &user, dbErr
}

func GetUserByEmail(email string) (u *User, err error) {
	var user User
	o := orm.NewOrm()
	dbErr := o.QueryTable("user").Filter("email", email).One(&user)
	return &user, dbErr
}

func HashString(string string) string {
	hasher := sha1.New()
	hasher.Write([]byte(string))

	hash := base64.StdEncoding.EncodeToString(hasher.Sum(nil))
	fmt.Println(hash)

	return hash
}

func Login(email string, password string) (bool, string) {
	token_hash := HashString(email + password)
	password_hash := HashString(password)

	fmt.Printf("Searching for user with email '%s', password: %s, token: %s", email, password_hash, token_hash)
	user, err := GetUserByEmail(email)

	if err == orm.ErrMultiRows {
		// Have multiple records
		fmt.Printf("Login: Returned Multi Rows Not One, failing  \n")
		return false, ""
	}
	if err == orm.ErrNoRows {
		fmt.Printf("Login: No user found, creating one \n")
		var newUser User
		newUser.Email = email
		newUser.Password = password_hash
		newUser.Token = token_hash

		o := orm.NewOrm()
		id, err := o.Insert(&newUser)
		if err == nil {
			fmt.Printf("Login: Created user with id: %d \n", id)
			user = &newUser
		} else {
			println(err)
		}
		
	}

	if user != nil && user.Password == password_hash {
		return true, user.Token
	}
	return false, ""
}

func DeleteUser(uid string) {

}
