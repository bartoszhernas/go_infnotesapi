package models

func init() {
}

type Note struct {
	Id   int
	Title string `orm:"size(255)"`
	Text string
	User *User `orm:"rel(fk);on_delete(do_nothing)"`
}
