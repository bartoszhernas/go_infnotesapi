#!/bin/sh
docker rmi infnotes
docker build -t infnotes  .
docker save infnotes > infnotes.tar
rsync ./infnotes.tar root@104.131.190.171:/var/tmp/infnotes.tar
rm infnotes.tar
ssh root@104.131.190.171 "docker load infnotes < /var/tmp/infnotes.tar"