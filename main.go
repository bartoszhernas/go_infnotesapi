package main

import (
	_ "infnotesapi/docs"
	_ "infnotesapi/routers"
	"infnotesapi/models"
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	// register model
	orm.RegisterModel(new(models.User), new(models.Note))
	orm.RegisterDriver("sqlite", orm.DR_Sqlite)
	orm.RegisterDataBase("default", "sqlite3", "database/orm_dev.db")
}


func main() {
	if beego.RunMode == "dev" {
		orm.Debug = true
		beego.DirectoryIndex = true
		beego.StaticDir["/swagger"] = "swagger"
	}
	//Database init
	err := orm.RunSyncdb("default", true, false)
	if err != nil {
		fmt.Println(err)
	}
	beego.Run()
}
